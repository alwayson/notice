import React, { Component } from 'react';
import Base from './Containers/Base';

class App extends Component {

  render() {
    return (
      <React.Fragment>
        <Base />
      </React.Fragment>
    );
  }
}

export default App;
