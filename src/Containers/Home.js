import React, { Component } from 'react';
import '../Home.css';
import { connect } from 'react-redux'
import Map from '../Components/Map'
import LoginSheet from '../Components/Login/LoginSheet'
import Commander from '../Containers/Commander'
import { CornerDialog } from 'evergreen-ui'
import { firebase } from '../firebase';
import { toggleSideSheetDrawer, changingLoginStatus, loginUser } from '../Actions/user'
import BottomSheet from '../Components/BottomSheet';
import NoticeCreator from '../Containers/NoticeCreator'
import * as config from '../config.json'

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cookies: false,
      authUser: null 
    }
    this.db = firebase.db;
    this.activateDemo = this.activateDemo.bind(this)
  }

  componentDidMount() {

    firebase.auth.onAuthStateChanged(authUser => {
      if (authUser && !this.props.status) {
        this.props.changingLoginStatus(authUser);
      }
    });
  }

  activateDemo() {    
    this.setState({ cookie: true })
    this.props.toggle();
    this.props.loginUser(config.demo.email,config.demo.password)
  }

  render() {

    return (
      <React.Fragment>
        <Commander />
        <CornerDialog
          title="👋 from Notice!"
          isShown={!this.state.cookie}
          width={492}
          onCloseComplete={() => this.setState({ cookie: true })}
          confirmLabel="Take me to the demo"
          cancelLabel="Log me in"
          onConfirm={this.activateDemo}
        >
          This is a product demonstration for <span role="img" aria-label="heart">💻</span> GovHack 2018, this is a real site you can use to your heart's desire or you can check out our demo.
        </CornerDialog>
        <Map style={{ overflowX: 'hidden', overflowY: 'hidden' }} db={this.db} />
        <LoginSheet
          visible={this.props.sidesheetVisible}
        />
        <BottomSheet/>
        <NoticeCreator/>
      </React.Fragment>

    );
  }
}

const mapStateToProps = state => ({
  ...state.user
})

const mapDispatchToProps = dispatch => ({
  toggle: () => dispatch(toggleSideSheetDrawer()),
  changingLoginStatus: (authUser) => dispatch(changingLoginStatus(authUser)),
  loginUser: (email, password) => dispatch(loginUser(email, password))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
