import React, { Component } from 'react';
import { connect } from 'react-redux'
import '../Home.css';
import { cancelCreatingNotice, startNominatingLocationForNotice, stopNominatingLocationForNotice, createNotice } from '../Actions/user'
import { Dialog, Paragraph, Badge, Button, CornerDialog, TextInput, Label, Textarea } from 'evergreen-ui';
import { Heading } from 'evergreen-ui/commonjs/typography';
import ImageUploader from 'react-images-upload';

class NoticeCreator extends Component {

    constructor(props) {

        super(props);

        this.state = {
            active: false,
            step: 0,
            noticeName: '',
            noticeBody: '',
            noticePicture: {
                data: '',
                name: ''
            }
        }

        this.handleChoice = this.handleChoice.bind(this)
        this.onDrop = this.onDrop.bind(this)
        
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleChoice(choice) {
        switch (choice) {
            case 'nominate':
                this.props.startNominatingLocationForNotice();
                this.setState({ step: 2 })
                break;
            case 'continue':
                this.setState({ step: 3 })
                break;
            case 'submit':
                this.props.createNotice({ name: this.state.noticeName || '', body: this.state.noticeBody || '', id: this.props.id || '', email: this.props.email || '', suburb: this.props.currentSuburb || '', image: this.state.noticePicture && this.state.noticePicture.data ? this.state.noticePicture.data : '', imageName: this.state.noticePicture && this.state.noticePicture.name ? this.state.noticePicture.name : ''})
                break;
            case 'done-nominating':
                this.props.stopNominatingLocationForNotice();
                this.setState({ step: 3 })
                break;
            default:
                break;

        }
    }

    componentWillReceiveProps(props) {
        if (this.props.creatingNotice !== props.creatingNotice && props.creatingNotice) {
            this.setState({ active: true, step: 1 })
        }
        else if (this.props.creatingNotice !== props.creatingNotice) {
            this.setState({ active: false, step: 0, noticeName: '', noticeBody: '' })
        }
    }

    onDrop(pictureFiles, pictureDataURLs) {

        var noticePicture = {};

        noticePicture.data = pictureDataURLs[pictureDataURLs.length-1]
        noticePicture.name = pictureFiles[pictureFiles.length-1].name

        this.setState({ noticePicture })

    }

    render() {

        return (
            <React.Fragment>
                <Dialog
                    isShown={this.state.step === 1}
                    hasFooter={false}
                    hasHeader={false}
                >
                    <Heading>
                        1. Noticeboard Location
                </Heading>
                    <br />
                    <Paragraph>
                        First things first, let's decide on somewhere to put your notice. Looks like you're near <Badge>{this.props.currentSuburb}</Badge>, sound good to you?
                </Paragraph>
                    <br />
                    <div style={{ textAlign: 'center' }}>
                        <Button
                            style={{ marginRight: 10 }}
                            id="nominate-location"
                            marginRight={12}
                            onClick={() => this.handleChoice("nominate")}>
                            Somewhere else
                        </Button>
                        <Button
                            id="sounds-good"
                            appearance='green'
                            marginRight={12}
                            onClick={() => this.handleChoice("continue")}>
                            Sounds good
                        </Button>
                    </div>
                </Dialog>
                <CornerDialog
                    title="1. Noticeboard Location"
                    isShown={this.state.step === 2}
                    onCloseComplete={() => this.handleChoice("done-nominating")}
                    hasCancel={false}
                    confirmLabel="I'm happy"
                >
                    Click on a location to nominate a suburb's noticeboard. How does <Badge>{this.props.currentSuburb}</Badge> sound?
        </CornerDialog>
                <Dialog
                    isShown={this.state.step === 3}
                    hasFooter={false}
                    hasHeader={false}
                >
                    <Heading>
                        2. Notice Details
                    </Heading>
                    <br />
                    <div style={{ textAlign: 'center' }}>
                        <Label htmlFor={40} size={500} display="block" marginBottom={4} marginTop={10}>
                            What would you like to call this notice?
                    </Label>
                        <TextInput
                            height={40}
                            name='noticeName'
                            id='noticeName'
                            placeholder="Title"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.noticeName} />
                        <Label htmlFor={40} size={500} display="block" marginBottom={4} marginTop={10}>
                            What is this notice all about?
                    </Label>
                        <Textarea
                            textSize={"400"}
                            name='noticeBody'
                            id='noticeBody'
                            placeholder="This is where you should describe the finer details of your notice"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.noticeBody} />
                        <Label htmlFor={40} size={500} display="block" marginBottom={4} marginTop={10}>
                            Event Image
                    </Label>
                        <ImageUploader
                            withIcon={false}
                            buttonStyles={{ className: 'button is-info' }}
                            withLabel={false}
                            onChange={this.onDrop}
                            buttonText={this.state.noticePicture && this.state.noticePicture.data && this.state.noticePicture.data.length > 0 ? this.state.noticePicture.name : 'Choose a picture'}
                            imgExtension={['.jpg', '.gif', '.png', '.gif']}
                            maxFileSize={1048487}
                        />
                    </div>
                    <div style={{ textAlign: 'center', marginTop: 10 }}>
                        <Button
                            isLoading={this.props.loadingNoticeCreating}
                            id="sounds-good"
                            appearance='green'
                            marginRight={12}
                            onClick={() => this.handleChoice("submit")}
                            withPreview={true}>
                            <span role="img" aria-label="confetti">🎉</span> All done
                        </Button>
                    </div>
                </Dialog>
            </React.Fragment >
        );
    }
}

const mapStateToProps = state => ({
    ...state.user
})

const mapDispatchToProps = dispatch => ({
    cancelCreatingNotice: () => dispatch(cancelCreatingNotice()),
    startNominatingLocationForNotice: () => dispatch(startNominatingLocationForNotice()),
    stopNominatingLocationForNotice: () => dispatch(stopNominatingLocationForNotice()),
    createNotice: (notice) => dispatch(createNotice(notice))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NoticeCreator)
