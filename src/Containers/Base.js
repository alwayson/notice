import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Home from './Home'
import Board from '../Components/Board'
import DB from '../Components/DB'
import * as Routes from '../Constants/routes'

class Base extends Component {

  render() {

    return (
      
      <React.Fragment>
        <Router>
          <Switch>
            <Route exact path={Routes.LANDING} component={Home} />
            <Route exact path={Routes.HOME} component={Home} />
            <Route exact path='/board' component={Board} />
            <Route exact path='/database' component={DB} />
          </Switch>
        </Router>
      </React.Fragment >
    );
  }
}

const mapStateToProps = state => ({
  ...state
})

export default connect(
  mapStateToProps
)(Base)
