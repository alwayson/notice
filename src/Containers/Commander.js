import React, { Component } from 'react';
import { connect } from 'react-redux'
import '../Home.css';
import { IconButton, Tooltip } from 'evergreen-ui';
import { toggleSideSheetDrawer, logoutUser } from '../Actions/user'
import ClickNHold from 'react-click-n-hold';

class Commander extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cookies: false,
      authUser: null
    }
    this.handleToggle = this.handleToggle.bind(this)
    this.handleLogout = this.handleLogout.bind(this)
  }

  handleToggle() {

    this.props.toggle();

  }

  handleLogout() {

    this.props.logoutUser();

  }


  render() {

    return (
      <div style={{ position: 'absolute', top: 40, right: 40, zIndex: 1000 }}>
        {this.props.status ? (
          <ClickNHold
            time={2}
            onEnd={this.handleLogout}
            style={{ position: 'absolute', right: 40 }}>
            <Tooltip content={`Hold and release to logout`}>
              <IconButton appearance="ghost" marginRight={12} icon="checkCircle" height={40} />
            </Tooltip>
          </ClickNHold>
        ) : null}
        <Tooltip content="Actions">
          <IconButton appearance="ghost" marginRight={12} icon="cog" onClick={this.handleToggle} height={40} />
        </Tooltip>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.user
})

const mapDispatchToProps = dispatch => ({
  toggle: () => dispatch(toggleSideSheetDrawer()),
  logoutUser: () => dispatch(logoutUser())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Commander)
