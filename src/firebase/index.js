import * as auth from './auth';
import * as firebase from './firebase';

// const firestore = firebase.firestore();
// const settings = {/* your settings... */ timestampsInSnapshots: true };
// firestore.settings(settings);

export {
  auth,
  firebase
};