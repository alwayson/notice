import firebase from 'firebase/app';
import 'firebase/auth'
import * as config from '../config.json'

if (!firebase.apps.length) {
  firebase.initializeApp(config.firebase);
}

const firestore = firebase.firestore();
const settings = {/* your settings... */ timestampsInSnapshots: true};
firestore.settings(settings);

const auth = firebase.auth();
const db = firebase.firestore();
const storage = firebase.storage().ref();
 
export {
  auth,
  db,
  storage
};