import firebase from 'firebase';
import * as config from '../config.json'
import { auth } from '../firebase'
import { db, storage } from '../firebase/firebase'
const uuidv1 = require('uuid/v1');


export const UPDATE_LOGIN_STATUS = 'UPDATE_LOGIN_STATUS';
export const UPDATE_REGISTER_STATUS = 'UPDATE_REGISTER_STATUS';
export const DISPLAY_MESSAGE = 'DISPLAY_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';
export const TOGGLE_SIDESHEET_DRAWER = 'TOGGLE_SIDESHEET_DRAWER';
export const SET_SUBURB = 'SET_SUBURB';
export const TOGGLE_SUBURB_NOTICEBOARD = 'TOGGLE_SUBURB_NOTICEBOARD';
export const LOADING_SUBURB = 'LOADING_SUBURB';
export const START_CREATING_NOTICE = 'START_CREATING_NOTICE'
export const CANCEL_CREATING_NOTICE = 'CANCEL_CREATING_NOTICE'
export const START_NOMINATING_LOCATION = 'START_NOMINATING_LOCATION'
export const STOP_NOMINATING_LOCATION = 'CANCEL_NOMINATING_LOCATION'
export const CREATED_NOTICE = 'CREATED_NOTICE'
export const SUBMIT_CREATE_NOTICE = 'SUBMIT_CREATE_NOTICE'

export function createNotice(notice) {

  return dispatch => {

    dispatch({ type: SUBMIT_CREATE_NOTICE })

    var storing = storage.child('notices/images/'+uuidv1()).putString(notice.image, 'data_url')

      storing.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        function (snapshot) {
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log('Upload is ' + progress + '% done');
          switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
              console.log('Upload is paused');
              break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
              console.log('Upload is running');
              break;
            default:
              console.log("Nothing")
              break;
          }
        }, function (error) {
          console.error(error.code)
        }
        , function () {

          storing.snapshot.ref.getDownloadURL().then(function (downloadURL) {
            console.log('File available at', downloadURL);

            db.collection('notices').add({
              name: notice.name,
              body: notice.body,
              owner: notice.id,
              contact: notice.email,
              board: notice.suburb,
              imageRef: downloadURL
            }).then(ref => {
              console.log('Added document with ID: ', ref.id);
              dispatch({ type: CREATED_NOTICE, data: { message: `Created a new notice for ${notice.suburb}`, messageType: 'success', messageVisible: true } })
            });
          });
        });
  }

}
export function loadingSuburb() {

  return dispatch => {

    dispatch({ type: LOADING_SUBURB });

  }
}

export function createEvent() {

  return dispatch => {

    dispatch({ type: LOADING_SUBURB });

  }
}

export function toggleSuburbNoticeboard(status) {

  return dispatch => {

    dispatch({ type: TOGGLE_SUBURB_NOTICEBOARD, data: status });

  }
}

export function setCurrentSuburb(suburb) {
  return dispatch => {

    dispatch({ type: SET_SUBURB, data: suburb });

  }
}

export function closeMessage() {
  return dispatch => {

    dispatch({ type: CLOSE_MESSAGE });

  }
}

export function startCreatingNotice() {

  return dispatch => {
    dispatch({ type: CANCEL_CREATING_NOTICE })
    setTimeout(() => {
      dispatch({ type: START_CREATING_NOTICE })
    }, 1000)

  }

}

export function startNominatingLocationForNotice() {

  return dispatch => {
    dispatch({ type: START_NOMINATING_LOCATION })
  }
}

export function stopNominatingLocationForNotice() {

  return dispatch => {
    dispatch({ type: STOP_NOMINATING_LOCATION })
  }
}

export function cancelCreatingNotice() {

  return dispatch => {
    dispatch({ type: CANCEL_CREATING_NOTICE })
  }

}

export function toggleSideSheetDrawer(status) {

  return dispatch => {
    dispatch({ type: TOGGLE_SIDESHEET_DRAWER, data: status })

  }

}

export function loginUser(email, password) {

  return dispatch => {

    var result = {};

    result.loading = true;
    result.status = false;
    result.messageVisible = false;

    dispatch({ type: UPDATE_LOGIN_STATUS, data: result })

    auth.doSignInWithEmailAndPassword(email, password)
      .then(authUser => {
        result.status = true;
        result.email = authUser.user.email;
        result.loading = false;
        result.id = authUser.user.uid;
        result.message = `Successfully logged ${result.email} in`;
        result.messageType = 'success';
        result.messageVisible = true;
        dispatch({ type: UPDATE_LOGIN_STATUS, data: result });
      })
      .catch(error => {
        result.messageType = 'error';
        result.loading = false;
        result.message = error.message;
        result.messageVisible = true;
        dispatch({ type: UPDATE_LOGIN_STATUS, data: result });
      });

  }

}

export function registerUser(email, password) {

  return dispatch => {

    var result = {};

    result.loading = true;
    result.status = false;

    dispatch({ type: UPDATE_REGISTER_STATUS, data: result })

    auth.doCreateUserWithEmailAndPassword(email, password)
      .then(authUser => {
        result.status = true;
        result.email = authUser.user.email;
        result.loading = false;
        result.id = authUser.user.uid;
        result.message = `Successfully created ${result.email} an account`;
        result.messageType = 'success';
        result.messageVisible = true;
        dispatch({ type: UPDATE_REGISTER_STATUS, data: result });
      })
      .catch(error => {
        result.messageType = 'error';
        result.status = false;
        result.message = error.message;
        result.loading = false;
        result.messageVisible = true;
        dispatch({ type: UPDATE_REGISTER_STATUS, data: result });
      });

  }

}

export function changingLoginStatus(authUser) {

  return dispatch => {

    var result = {};

    result.status = true;
    result.email = authUser.email;
    result.id = authUser.uid;
    result.loading = false;
    result.message = `We've logged ${result.email} back in`;
    result.messageType = 'success';
    result.messageVisible = true;

    dispatch({ type: UPDATE_LOGIN_STATUS, data: result });
  }

}


export function testLoginUser(appVerifier) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebase);
  }
  // Turn off phone auth app verification.
  firebase.auth().settings.appVerificationDisabledForTesting = true;

  var phoneNumber = "+61468360724";
  var testVerificationCode = "123456";

  // This will render a fake reCAPTCHA as appVerificationDisabledForTesting is true.
  // This will resolve after rendering without app verification.
  // signInWithPhoneNumber will call appVerifier.verify() which will resolve with a fake
  // reCAPTCHA response.
  firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
    .then(function (confirmationResult) {
      return confirmationResult.confirm(testVerificationCode);
    }).catch(function (error) {
      // Error; SMS not sent
      // ...
      return 'error';
    });
}
export function logoutUser() {

  return dispatch => {

    var result = {};

    firebase.auth().signOut().then(function () {

      result.status = false;
      result.email = "";
      result.loading = false;
      result.message = `We've logged you out`;
      result.messageType = 'success';
      result.messageVisible = true;

      dispatch({ type: UPDATE_LOGIN_STATUS, data: result });
    }).catch(function (error) {

      result.status = false;
      result.email = "";
      result.loading = false;
      result.message = `We haven't properly logged out out, please restart your browser`;
      result.messageType = 'error';
      result.messageVisible = true;

      dispatch({ type: UPDATE_LOGIN_STATUS, data: result });
    });

  }

}
