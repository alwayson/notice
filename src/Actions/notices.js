import { db } from '../firebase/firebase'

export const RETRIEVING_NOTICES = 'RETRIEVING_NOTICES'
export const RETRIEVED_NOTICES = 'RETRIEVED_NOTICES'

export function GetCards(suburb) {

    return async dispatch => {

        dispatch({ type: RETRIEVING_NOTICES });
        
        var noticesRef = db.collection('notices');
        var notices = JSON.stringify(await new Promise(function (resolve, reject) {
            noticesRef.where("board", "==", suburb).get()
            .then(async function (querySnapshot) {
                
                var temp = [];

                await querySnapshot.forEach(function (doc) {
                    temp.push(doc.data())
                });

                resolve(temp);

            })
            .catch(function (error) {
                reject(error);
                console.log("Error getting documents: ", error);
            });
        })); 
        dispatch({type: RETRIEVED_NOTICES, data: {notices: notices, suburb: suburb}})

    }
}