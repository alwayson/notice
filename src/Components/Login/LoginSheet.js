import React, { Component } from 'react';
import { connect } from 'react-redux'
import { loginUser, logoutUser, registerUser, closeMessage, startCreatingNotice } from '../../Actions/user'
import { Text, Button, SideSheet, Heading, TextInput, toaster } from 'evergreen-ui'
import Board from '../Board'

class LoginSheet extends Component {

  constructor(props) {
    super(props);

    this.state = {
      
      status: true,
      email: '',
      password: '',
      creating: false

    }
    this.handleLogin = this.handleLogin.bind(this)
    this.handleRegister = this.handleRegister.bind(this)
    this.handleCreate = this.handleCreate.bind(this)
  };

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleLogin() {
    this.props.login(this.state.email, this.state.password)
  }

  handleRegister() {
    this.props.register(this.state.email, this.state.password)
  }

  handleCreate() {
    this.props.startCreatingNotice()
  }

  componentWillReceiveProps(props) {

    if (props.messageVisible) {

      switch (props.messageType) {
        case 'error':
          toaster.danger(props.message)
          break;
        case 'success':
          toaster.success(props.message)
          break;
        default:
          toaster.info("Something is going on, we're working on it")
      }

      this.props.closeMessage();

    }

    if (props.status !== this.props.status) {

      this.setState({ status: props.status })

    }

  }

  render() {

    var primary;

    // Switch for authentication
    switch (this.props.status) {
      case true:
        primary = (
          <div>
            <Heading style={{ paddingBottom: 40 }} size={900}> <span role="img" aria-label="city">🌆</span> Notice</Heading>
            <Heading size={500}>Description</Heading>
            <Text size={400}>Welcome to the gang! This web-app is all about promoting usage of local public spaces to reignite local communities and getting rid of those pesky noticeboards at the local shops where each print-off blends into one. Have a shot at making an event or notice to put up on your local board.</Text>
            <div style={{ paddingTop: 40, textAlign: 'center' }}>
              <Button
                isLoading={this.props.loginLoading}
                style={{ marginTop: 10 }}
                id="login-button"
                marginRight={12}
                appearance="green"
                onClick={this.handleCreate}>
                Create a Notice
            </Button>
              <div style={{ paddingTop: 40, paddingBottom: 40 }}>
                <Text style={{ paddingTop: 40 }}>Made with <span role="img" aria-label="heart">❤️</span> by Josh and Sam</Text>
              </div>
            </div>
          </div>
        )
        break;
      default:
        primary = (
          <div>
            <Heading style={{ paddingBottom: 40 }} size={900}><span role="img" aria-label="city">🌃</span> Notice</Heading>
            <Text size={400}>Welcome to Notice! <span role="img" aria-label="smile">😊</span> Please login to be able to deploy, view and manage your notices for your local community.</Text>
            <div style={{ paddingTop: 40 }}>
              <div style={{ textAlign: 'center' }}>
                <TextInput
                  name={'email'}
                  id={'email'}
                  placeholder="Email Address"
                  onChange={(e) => this.handleChange(e)}
                  value={this.state.email}
                />
                <div style={{ paddingTop: 10 }}>
                  <TextInput
                    name={'password'}
                    id={'password'}
                    type='password'
                    placeholder="Password"
                    onChange={(e) => this.handleChange(e)}
                    value={this.state.password}
                  />
                </div>
                <br />
                <Button
                  isLoading={this.props.loginLoading}
                  style={{ marginTop: 10 }}
                  id="login-button"
                  marginRight={10}
                  appearance="green"
                  onClick={this.handleLogin}>
                  Login
          </Button>
                <Button
                  style={{ marginTop: 10 }}
                  id="logup-button"
                  marginRight={10}
                  appearance="blue"
                  isLoading={this.props.registerLoading}
                  onClick={this.handleRegister}>
                  Register
          </Button>
              </div>
              <div style={{ paddingTop: 40 }}>
                <Board
                  width='100%'
                  animationDelay={200}
                  showCards={!this.props.loginLoading}
                  shouldAnimate={true}
                />
              </div>
              <div style={{ paddingTop: 40, paddingBottom: 40, textAlign: 'center' }}>
                <Text style={{ paddingTop: 40 }}>Made with <span role="img" aria-label="heart">❤️</span> by Josh and Sam from Always O(n)</Text>
              </div>
            </div>
          </div>)
        break;
    }

    return (
      <div className='container'>

        <SideSheet
          isShown={this.props.visible}
        >

          <div style={{ paddingLeft: 40, paddingTop: 40, paddingRight: 40 }}>
            {primary}
          </div>
        </SideSheet>

        {}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.user
})

const mapDispatchToProps = dispatch => ({
  login: (email, password) => dispatch(loginUser(email, password)),
  logout: () => dispatch(logoutUser()),
  register: (email, password) => dispatch(registerUser(email, password)),
  closeMessage: () => dispatch(closeMessage()),
  startCreatingNotice: () => dispatch(startCreatingNotice())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginSheet)