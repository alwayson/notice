import React, { Component } from 'react';
import { connect } from 'react-redux'
import { loginUser, logoutUser, registerUser, closeMessage } from '../Actions/user'
import { Text, Button, SideSheet, Heading, TextInput, toaster } from 'evergreen-ui'
import Board from './Board'

class Dashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            state: '',
            email: '',
            password: '',
            tab: 'login'
        }

    };

    render() {

        return (

            <div>

                <Board/>

            </div>

    );

    }

}

const mapStateToProps = state => ({
    ...state.user
})

const mapDispatchToProps = dispatch => ({
    login: (email, password) => dispatch(loginUser(email, password)),
    logout: () => dispatch(logoutUser()),
    register: (email, password) => dispatch(registerUser(email, password)),
    closeMessage: () => dispatch(closeMessage)
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard)