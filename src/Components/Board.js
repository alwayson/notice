import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Heading, Paragraph, toaster } from 'evergreen-ui';
import { GetCards } from '../Actions/notices'

import * as data from './demoData.json'

class Board extends Component {

  constructor(props) {
    super(props)

    this.state = {

      rendered: false,
      buckets: [],
      suburb: ''

    }

    this.loadCards = this.loadCards.bind(this)
  }

  loadCards(input_cards) {

    console.log(input_cards)

    var temp = JSON.parse(JSON.stringify(input_cards))
    temp = temp.map(c => {return {...c, width: 3}})
    console.log(temp)

    var cards = [...temp];

    this.setState({ rendered: true, buckets: [[...temp]] })
  }

  choose(choices) {

    var index = Math.floor(Math.random() * choices.length);
    return choices[index];

  }

  randomDate() {
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    return this.choose(months) + ' ' + (Math.floor(Math.random() * 30) + 1);
  }

  componentWillReceiveProps(props) {

    if (this.props.user.currentSuburb !== props.user.currentSuburb || (props.user.currentSuburb && !props.notices.fetched && !props.notices.fetching)) {

      console.log("RETRIEVING NEW CARDS FOR " + props.user.currentSuburb)
      this.setState({ rendered: false, buckets: [] })
      this.props.GetCards(props.user.currentSuburb)

    }

    if (props.notices.fetched && !props.notices.fetching && props.notices.notices.length > 0 && this.state.rendered === false) {

      this.loadCards(props.notices.notices);

    }

  }

  render() {


    let { heading, suburb, animationDelay, showCards, shouldAnimate } = this.props;
    let cardAnimationStyle = () => { };
    let cardAnimationClassName = '';
    let headingAnimationClassName = '';
    showCards = showCards === undefined ? true : showCards;
    shouldAnimate = shouldAnimate === undefined ? true : shouldAnimate;
    animationDelay = animationDelay === undefined ? 0 : animationDelay;

    if (showCards && shouldAnimate) {
      cardAnimationStyle = function (i) { return { animationDelay: animationDelay + i * 70 + 'ms' } };
      cardAnimationClassName = 'animated bounceIn';
      headingAnimationClassName = 'animated slideInDown';
    } else if (!showCards && shouldAnimate) {
      cardAnimationClassName = 'animated fadeOut';
      headingAnimationClassName = 'animated slideOutLeft';
      // cardAnimationStyle = function (i) { return {maxHeight:'min-content' } };
    } else if (!showCards && !shouldAnimate) {
      cardAnimationClassName = 'is-hidden';
    }

    const pinStyle = (n, colour) => `.pin${n}:after {background-color: ${colour};}`;
    let pinColours = ['crimson', 'orangered', 'yellow', 'violet', 'rebeccapurple', 'limegreen', 'forestgreen', 'turquoise', 'dodgerblue', 'navy', 'darkslategray']

    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
    var i = 1;

    return (
      <div className='container' style={{ maxWidth: this.props.width }} >
        <Heading size={900} className={headingAnimationClassName}>{heading}</Heading>
        <Heading size={700} className={headingAnimationClassName}>{suburb}</Heading>
        <hr></hr>
        <div className='tile is-ancestor is-vertical'>
          {
            this.state.buckets.map(bucket => {
              return (
                <div className='tile is-parent is-paddingless' key={guid()}>
                  {
                    bucket.map(element => {
                      return (
                        <div className={cardAnimationClassName + ' has-shadow tile is-parent is-' + element.width} key={guid()} style={cardAnimationStyle(i)}>
                          {i++ && null}
                          <article className={'tile is-child notification is-' + element.colour} style={{ padding: '0.5em' }}>
                            <style>
                              {pinStyle(i, this.choose(pinColours))}
                            </style>
                            <i className={'pin pin' + i}></i>
                            <header>
                              <Heading size={700} color='inherit'>{element.name}</Heading>
                            </header>
                            <Paragraph color='inherit'>{element.body}</Paragraph>
                            {(element.imageRef !== '' && element.imageRef) &&
                              <center>
                                {/* <figure class="image" style={{overflowY:'auto', maxWidth:'256px', maxHeight:'256px'}}> */}
                                <figure class="image" >
                                  <img src={element.imageRef} ></img>
                                </figure>
                              </center>
                            }
                            <hr style={{ marginTop: '5px', marginBottom: '5px' }}></hr>
                            <footer>
                              <span className='is-pulled-left'>
                                <i className='fas fa-calendar-alt'></i>
                                <span style={{ marginRight: '10px' }}></span>
                                {/* <time datetime='2016-1-1'>1 Sep</time> */}
                                <span>{this.randomDate()}</span>
                              </span>

                              {/* <span className='is-pulled-right'>
                                <i className='fas fa-user'></i>
                                <span style={{ marginRight: '10px' }}></span>
                                Firstman Lastnamey
                                </span> */}
                            </footer>
                          </article>
                        </div>
                      )
                    })
                  }
                </div>
              )
            })
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state
})

const mapDispatchToProps = dispatch => ({
  GetCards: (suburb) => dispatch(GetCards(suburb))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Board)