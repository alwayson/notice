import React, { Component } from 'react';
import { firestore } from 'firebase';
import { firebase } from '../firebase';

class DB extends Component {

  constructor() {

    super()
      
      this.state = {
        error: null,
        isLoaded: false,
        items: []
      }
      const settings = {/* your settings... */ timestampsInSnapshots: true };
      firestore.settings(settings);
      this.db = firebase.db;

  }
    
    bbqs = () => {
      fetch("https://www.data.act.gov.au/resource/ti8x-vy4v.json")
        .then(res => res.json())
        .then(
          (result) => {
            result.forEach((e) => {
              this.db.collection("locations").add({
                name: e.location_description,
                type: "BBQ",
                bbq_type: e.bbq_type,
                suburb : e.location,
                location:  new firestore.GeoPoint(parseFloat(e['latitude']), parseFloat(e['longitude']))
              })
            })
          },
          (error) => {  
            console.log(error)  
          }
        )
    }

    furniture = () => {
      fetch("https://www.data.act.gov.au/resource/knzq-ianp.json")
        .then(res => res.json())
        .then(
          (result) => {
            result.forEach((e) => {
              this.db.collection("locations").add({
                name: e.location_name, 
                type: "FURNITURE",
                furniture_type: e.feature_type,
                suburb: e.division_name,
                location:  new firestore.GeoPoint(parseFloat(e['latitude']), parseFloat(e['longitude']))
              })
            })
          },
          (error) => {  
            console.log(error)  
          }
        )
    }

    bball = () => {
      fetch("https://www.data.act.gov.au/resource/sde8-8gwg.json")
      .then(res => res.json())
      .then(
        (result) => {
          result.forEach((e) => {
            this.db.collection("locations").add({
              name: e.location, 
              type: "BBALL",
              bball_type: e.type,
              suburb: e.division,
              location:  new firestore.GeoPoint(parseFloat(e['latitude']), parseFloat(e['longitude']))
            })
          })
        },
        (error) => {  
          console.log(error)  
        }
      )
    }

    fitness = () => {
      fetch("https://www.data.act.gov.au/resource/4x3y-6wz4.json")
      .then(res => res.json())
      .then(
        (result) => {
          result.forEach((e) => {
            this.db.collection("locations").add({
              name: e.location, 
              type: "FITNESS",
              fitness_type: e.type,
              suburb: e.division,
              location:  new firestore.GeoPoint(parseFloat(e['latitude']), parseFloat(e['longitude']))
            })
          })
        },
        (error) => {  
          console.log(error)  
        }
      )
    }

    dogs = () => {
      fetch("https://www.data.act.gov.au/resource/agh7-cjq8.json")
      .then(res => res.json())
      .then(
        (result) => {
          result.forEach((e) => {
            this.db.collection("locations").add({
              name: e.location, 
              type: "DOG",
              suburb: e.division,
              location:  new firestore.GeoPoint(parseFloat(e['latitude']), parseFloat(e['longitude']))
            })
          })
        },
        (error) => {  
          console.log(error)  
        }
      )
    }

    parks = () => {
      fetch("https://www.data.act.gov.au/resource/9y36-yxpi.json")
      .then(res => res.json())
      .then(
        (result) => {
          result.forEach((e) => {
            this.db.collection("locations").add({
              name: e.location, 
              type: "PARK",
              park_type: e.equipment,
              suburb: e.division_name,
              location:  new firestore.GeoPoint(parseFloat(e['latitude']), parseFloat(e['longitude']))
            })
          })
        },
        (error) => {  
          console.log(error)  
        }
      )
    }

    skateparks = () => {
      fetch("https://www.data.act.gov.au/resource/5yva-w7wu.json")
      .then(res => res.json())
      .then(
        (result) => {
          result.forEach((e) => {
            this.db.collection("locations").add({
              name: e.location, 
              type: "SKATEPARK",
              suburb: e.division,
              location:  new firestore.GeoPoint(parseFloat(e['latitude']), parseFloat(e['longitude']))
            })
          })
        },
        (error) => {  
          console.log(error)  
        }
      )
    }
    
    
    render() {
      return (
        <React.Fragment>
        <button onClick={this.bbqs}>
          bbqs
        </button>

        <button onClick={this.furniture}>
          public furniture
        </button>

        <button onClick={this.bball}>
          baskette ball
        </button>

        <button onClick={this.fitness}>
          fitness
        </button>
        
        <button onClick={this.dogs}>
          doggo parks
        </button>

        <button onClick={this.parks}>
          parks
        </button>

        <button onClick={this.skateparks}>
          skate parks
        </button>
        </React.Fragment>
      );

    };
     
  }

export default DB;
