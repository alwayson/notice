import React, { Component } from 'react';
import { connect } from 'react-redux'
import ReactMapGL, { Marker, FlyToInterpolator } from 'react-map-gl';
import * as config from '../config.json'
import { setCurrentSuburb, toggleSuburbNoticeboard, loadingSuburb } from '../Actions/user'
import { toaster } from 'evergreen-ui'
import WebMercatorViewport from 'viewport-mercator-project';
import * as d3 from 'd3-ease';
const mbxGeocoding = require('@mapbox/mapbox-sdk/services/geocoding');
const geocodingClient = mbxGeocoding({ accessToken: config.mapbox.token });

class Map extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeMode: 'default',
      points: [],
      width: 0,
      height: 0,
      longitude: 0,
      latitude: 0,
      displayCurrentLocation: this.props.displayCurrentLocation,
      viewport: {
        latitude: -35.2809370,
        longitude: 149.1300090,
        zoom: 10,
        bearing: 0,
        pitch: 0,
        width: 500,
        height: 500,
        compact: true
      },
      values: [],
      selectedLongitude: 0,
      selectedLatitude: 0,
    };

    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.getMyLocation = this.getMyLocation.bind(this)
    this.geocodeLocation = this.geocodeLocation.bind(this)

  }

  nominateLocation(longitude, latitude) {

    this.geocodeLocation(longitude, latitude, 'nominating')

  }

  // Retrieves the suburb that you click on
  geocodeLocation(longitude, latitude, mode) {

    this.props.loadingSuburb();

    var promises = [];

    promises.push(geocodingClient
      .reverseGeocode({
        query: [longitude, latitude],
        limit: 1
      })
      .send()
      .then(response => {
        // match is a GeoJSON document with geocoding matches
        const match = response.body;

        var suburb = '';
        var country = '';

        if (match.features && match.features[0]) {
          suburb = match.features[0].context[0] ? match.features[0].context[0].text : ''
          country = match.features[0].context[4] ? match.features[0].context[4].text : ''
        }
        if (!hasNumber(suburb) && suburb.length > 0) {

          return [suburb, country];
        }
        else {
          return null
        }
      }))

    promises.push(new Promise(function (resolve, reject) {
      setTimeout(resolve, 800);
    }))

    promises.push(new Promise(function (resolve, reject) {
      setTimeout(resolve, 800);
    }))

    var suburb;
    var country;

    Promise.all(promises).then((values) => {
      suburb = values[0] ? values[0][0] : '';
      country = values[0] ? values[0][1] : '';

      if (suburb.length > 0) {

        if (this.props.user.currentSuburb !== suburb && mode !== 'nominating') {
          toaster.success(`Setting ${suburb} as your active suburb`)
        }

        this.props.setCurrentSuburb(suburb)

        if (!this.props.suburbNoticeboardVisible && mode !== 'current-location' && mode !== 'nominating') {
          this.props.toggleSuburbNoticeboard(true)
        }

        (geocodingClient
          .forwardGeocode({
            query: `${suburb}, ${country}`,
            limit: 1
          })
          .send()
          .then(response => {
            const match = response.body;

            var viewport = { ...this.state.viewport }

            const { longitude, latitude, zoom } = new WebMercatorViewport(this.state.viewport)
              .fitBounds([[match.features[0].bbox[0], match.features[0].bbox[1]], [match.features[0].bbox[2], match.features[0].bbox[3]]], {
                padding: 20,
                offset: [0, 0]
              });

            viewport.latitude = latitude
            viewport.longitude = longitude
            viewport.zoom = zoom;
            viewport.transitionDuration = 5000
            viewport.transitionInterpolator = new FlyToInterpolator()
            viewport.transitionEasing = d3.easeCubic
            this.setState({ viewport })
          })
        )
      }

      else {
        toaster.danger(`Uh oh, we couldn't find a nearby noticeboard here`)
      }
    });
  }

  componentDidMount() {

    this.getMyLocation()

    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);

    this.props.db.collection("locations").limit(50).get().then((querySnapshot) => {
      this.setState({ points: querySnapshot.docs })

      var dic = {
        "BBQ": "fas fa-map-pin",
        "FURNITURE": "fas fa-utensils",
        "BBALL": "fas fa-basketball-ball",
        "FITNESS": "fas fa-dumbbell",
        "DOG": "fas fa-frog",
        "PARK": "fas fa-tree",
        "SKATEPARK": "fas fa-capsules"
      }

      function guid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
      }

      if (this.state.values.length === 0) {
        var values = this.state.points.map(e => <Marker key={guid()} latitude={e.data().location.latitude} longitude={e.data().location.longitude} offsetLeft={-20} offsetTop={-10}><i className={dic[e.data().type]}></i></Marker>)
        this.setState({ values: values })
      }

    });

  }

  updateWindowDimensions() {
    var viewport = { ...this.state.viewport }
    viewport.width = window.innerWidth;
    viewport.height = window.innerHeight;

    this.setState({ viewport });

  }

  getMyLocation() {

    if (!this.props.user.currentSuburb) {
      toaster.notify("Locating your nearest suburb")
    }

    const location = window.navigator && window.navigator.geolocation

    if (location) {

      location.getCurrentPosition((position) => {
        this.geocodeLocation(position.coords.longitude, position.coords.latitude, 'current-location')
      })

    }

  }

  render() {

    return (
      <React.Fragment>
        <ReactMapGL
          {...this.state.viewport}
          ref={(map) => { this.map = map; }}
          mapboxApiAccessToken={config.mapbox.token}
          mapStyle="mapbox://styles/jbwhitcombe/cjlt1dya805nr2skyzvz5g9a7"
          onViewportChange={(viewport) => this.setState({ viewport })}
          onClick={event => {
            if (this.props.user.nominatingLocation) {
              this.nominateLocation(event.lngLat[0], event.lngLat[1])
            }
            else {
              this.geocodeLocation(event.lngLat[0], event.lngLat[1])
            }

          }
          }
        >

          {this.state.values}

        </ReactMapGL>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  ...state
})
const mapDispatchToProps = dispatch => ({
  setCurrentSuburb: (suburb) => dispatch(setCurrentSuburb(suburb)),
  toggleSuburbNoticeboard: (status) => dispatch(toggleSuburbNoticeboard(status)),
  loadingSuburb: () => dispatch(loadingSuburb())
})

function hasNumber(myString) {
  return /\d/.test(myString);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Map)