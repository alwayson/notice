import React, { Component } from 'react';
import { IconButton } from 'evergreen-ui';
import { connect } from 'react-redux'
import SwipeableBottomSheet from 'react-swipeable-bottom-sheet';
import { toggleSuburbNoticeboard } from '../Actions/user';
import Board from './Board';




class BottomSheet extends Component {

  constructor (props) {
    super(props);
    this.state = {
      sheetVisible: false
    };

    this.toggle = this.toggle.bind(this);

  }

  componentDidMount () {

  }

  toggle () {
    this.setState({ sheetVisible: !this.state.sheetVisible })
  }


  render () {

    const sheetContent = (

      <React.Fragment>
        <IconButton appearance="ghost" marginLeft={20} marginTop={20} icon="close" onClick={() => this.props.toggle(false)} />
        {this.props.currentSuburb ? 
        <Board
          width='100%'
          animationDelay={100}
          showCards={!this.props.loadingSuburb}
          shouldAnimate={true}
          heading={this.props.currentSuburb}
          suburb={'Notices'}/>
        :
        null
        }
      </React.Fragment>

    );

    return (
          <SwipeableBottomSheet
            open={this.props.suburbNoticeboardVisible}
            overlay={false}
            onChange={open => this.props.toggle(open)} >
            <div style={{ height: '65vh' }}>{sheetContent}</div>
          </SwipeableBottomSheet>
    );
  }
}

const mapStateToProps = state => ({
  ...state.user
})

const mapDispatchToProps = dispatch => ({
  toggle: () => dispatch(toggleSuburbNoticeboard())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BottomSheet)