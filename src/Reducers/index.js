import { combineReducers } from 'redux'
import user from './user'
import notices from './notices'

const notice = combineReducers({
    user,
    notices
})

export default notice