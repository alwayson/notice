import { RETRIEVED_NOTICES, RETRIEVING_NOTICES } from '../Actions/notices'

const user = (state = [], action) => {
    switch (action.type) {
        case RETRIEVED_NOTICES:
            return {...state, suburb: action.data.suburb, fetched: true, fetching: false, notices: JSON.parse(action.data.notices)}
        case RETRIEVING_NOTICES:
            return {...state, suburb: '', fetched: false, fetching: true, notices: []}
        default:
            return {...state}
    }
}

export default user