import { SUBMIT_CREATE_NOTICE, CREATED_NOTICE, STOP_NOMINATING_LOCATION, START_NOMINATING_LOCATION, CANCEL_CREATING_NOTICE, START_CREATING_NOTICE, LOADING_SUBURB, TOGGLE_SUBURB_NOTICEBOARD, SET_SUBURB, UPDATE_LOGIN_STATUS, UPDATE_REGISTER_STATUS, DISPLAY_MESSAGE, CLOSE_MESSAGE, TOGGLE_SIDESHEET_DRAWER } from '../Actions/user'

const user = (state = [], action) => {
    switch (action.type) {
        case UPDATE_LOGIN_STATUS:
            return ({...state, email: action.data.email, id: action.data.id, status: action.data.status, loginLoading: action.data.loading, messageType: action.data.messageType, message: action.data.message, messageVisible: action.data.messageVisible})
        case UPDATE_REGISTER_STATUS:
            return ({...state, email: action.data.email, id: action.data.id, status: action.data.status, registerLoading: action.data.loading, messageType: action.data.messageType, message: action.data.message, messageVisible: action.data.messageVisible})
        case DISPLAY_MESSAGE:
            return ({...state, messageType: action.data.messageType, message: action.data.message, messageVisible: true})
        case CLOSE_MESSAGE:
            return ({...state, messageVisible: false})
        case SET_SUBURB:
            return ({...state, currentSuburb: action.data, loadingSuburb: false})
        case TOGGLE_SUBURB_NOTICEBOARD:
            return ({...state, suburbNoticeboardVisible: action.data})
        case LOADING_SUBURB:
            return ({...state, loadingSuburb: true})
        case SUBMIT_CREATE_NOTICE:
            return ({...state, loadingNoticeCreating: true})
        case CREATED_NOTICE:
            return ({...state, loadingNoticeCreating: false, messageType: action.data.messageType, message: action.data.message, messageVisible: true, creatingNotice: false})
        case START_CREATING_NOTICE:
            return ({...state, creatingNotice: true, suburbNoticeboardVisible: false, sidesheetVisible: false})
        case CANCEL_CREATING_NOTICE:
            return ({...state, creatingNotice: false})
        case TOGGLE_SIDESHEET_DRAWER:
            if (action.data === undefined) {
                return ({...state, sidesheetVisible: !state.sidesheetVisible || false})
            }
            else {
                return ({...state, sidesheetVisible: action.data})
            }
        case START_NOMINATING_LOCATION:
            return ({...state, nominatingLocation: true})
        case STOP_NOMINATING_LOCATION:
            return ({...state, nominatingLocation: false})

        default:
            return state
    }
}

export default user